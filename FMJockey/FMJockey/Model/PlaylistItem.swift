//
//  PlaylistItem.swift
//  FMJockey
//
//  Created by MuraliKrishna on 14/07/18.
//  Copyright © 2018 Radio Choklate. All rights reserved.
//

import UIKit

class PlaylistItem {
    let playListId:String
    let playListTitle:String
    let playListThumbNailUrl:String
    let arrayVideoItems:[VideoItem]?
    init(playListId:String?,playListTitle:String?,playListThumbNailUrl:String?,arrayVideoItems:[VideoItem]?) {
        self.playListId = playListId ?? ""
        self.playListTitle = playListTitle ?? ""
        self.playListThumbNailUrl = playListThumbNailUrl ?? ""
        self.arrayVideoItems = arrayVideoItems
    }
}

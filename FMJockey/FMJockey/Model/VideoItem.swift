//
//  VideoItem.swift
//  FMJockey
//
//  Created by MuraliKrishna on 14/07/18.
//  Copyright © 2018 Radio Choklate. All rights reserved.
//

import UIKit

class VideoItem {
    init(videoId:String?, thumbnailUrl:String?,title:String?,description:String?) {
        self.videoId = videoId
        self.thumbnailUrl = thumbnailUrl
        self.title = title
        self.description = description
        
    }
    let videoId:String?
    let thumbnailUrl:String?
    let description:String?
    let title:String?
    
}

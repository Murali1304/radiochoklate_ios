//
//  NetworkingManager.swift
//  FMJockey
//
//  Created by MuraliKrishna on 08/07/18.
//  Copyright © 2018 Radio Choklate. All rights reserved.
//

import UIKit
extension URLSession {
    func synchronousDataTask(with url: URL) -> (Data?, URLResponse?, Error?) {
        var data: Data?
        var response: URLResponse?
        var error: Error?
        
        let semaphore = DispatchSemaphore(value: 0)
        
        let dataTask = self.dataTask(with: url) {
            data = $0
            response = $1
            error = $2
            
            semaphore.signal()
        }
        dataTask.resume()
        
        _ = semaphore.wait(timeout: .distantFuture)
        
        return (data, response, error)
    }
}
class NetworkingManager {
static let sharedManager = NetworkingManager()
    func performAsyncGetRequest(_ urlString: String, completion: @escaping (_ data: Data?, _ HTTPStatusCode: Int?, _ error: Error?) -> Void) {
        guard let url = URL(string: urlString) else {
            print("Error: cannot create URL")
            return
        }
        let urlRequest = URLRequest(url: url)
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in

            DispatchQueue.main.async(execute: { () -> Void in
                completion(data,(response as? HTTPURLResponse)?.statusCode, error)
            })
        }
        task.resume()
        
    }
        
    func performGetRequest(_ urlString: String, completion: @escaping (_ data: Data?, _ HTTPStatusCode: Int?, _ error: Error?) -> Void) {
        
        guard let url = URL(string: urlString) else {
            print("Error: cannot create URL")
            return
        }
//        let urlRequest = URLRequest(url: url)
//
//        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
//        // make the request
//        let task = session.dataTask(with: urlRequest) {
//            (data, response, error) in
//
//            DispatchQueue.main.async(execute: { () -> Void in
//                completion(data,(response as! HTTPURLResponse).statusCode, error)
//            })
      let (data, response, error) =  session.synchronousDataTask(with: url)
        DispatchQueue.main.async(execute: { () -> Void in
           
            completion(data,(response as? HTTPURLResponse)?.statusCode, error)
        
        })
        
        
 /*           // check for any errors
            guard error == nil else {
                completionHandler(nil,error)
//                print("error calling GET on /todos/1")
//                print(error!)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: [])
                    as? [String: Any] else {
                        print("error trying to convert data to JSON")
                        return
                }
                // now we have the todo
                // let's just print it to prove we can access it
                print("The todo is: " + todo.description)
                
//                // the todo object is a dictionary
//                // so we just access the title using the "title" key
//                // so check for a title and print it if we have one
//                guard let todoTitle = todo["title"] as? String else {
//                    print("Could not get todo title from JSON")
//                    return
//                }
//                print("The title is: " + todoTitle)
            } catch  {
                print("error trying to convert data to JSON")
                return
            }*/
//        }
//        task.resume()
    }
    
    func makePostCall() {
        let todosEndpoint: String = "https://jsonplaceholder.typicode.com/todos"
        guard let todosURL = URL(string: todosEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        var todosUrlRequest = URLRequest(url: todosURL)
        todosUrlRequest.httpMethod = "POST"
        let newTodo: [String: Any] = ["title": "My First todo", "completed": false, "userId": 1]
        let jsonTodo: Data
        do {
            jsonTodo = try JSONSerialization.data(withJSONObject: newTodo, options: [])
            todosUrlRequest.httpBody = jsonTodo
        } catch {
            print("Error: cannot create JSON from todo")
            return
        }
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: todosUrlRequest) {
            (data, response, error) in
            guard error == nil else {
                print("error calling POST on /todos/1")
                print(error!)
                return
            }
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            
            // parse the result as JSON, since that's what the API provides
            do {
                guard let receivedTodo = try JSONSerialization.jsonObject(with: responseData,
                                                                          options: []) as? [String: Any] else {
                                                                            print("Could not get JSON from responseData as dictionary")
                                                                            return
                }
                print("The todo is: " + receivedTodo.description)
                
                guard let todoID = receivedTodo["id"] as? Int else {
                    print("Could not get todoID as int from JSON")
                    return
                }
                print("The ID is: \(todoID)")
            } catch  {
                print("error parsing response from POST on /todos")
                return
            }
        }
        task.resume()
    }

}

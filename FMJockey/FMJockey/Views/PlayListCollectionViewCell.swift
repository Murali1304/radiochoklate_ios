//
//  PlayListCollectionViewCell.swift
//  FMJockey
//
//  Created by MuraliKrishna on 14/07/18.
//  Copyright © 2018 Radio Choklate. All rights reserved.
//

import UIKit

class PlayListCollectionViewCell: UICollectionViewCell {
    @IBOutlet var playListThumbImageView:UIImageView!
    @IBOutlet var playListTitle:UILabel!
    func setContentFor(imageUrlString:String?,title:String?)  {
        self.playListTitle.text = title
        self.playListThumbImageView.image = UIImage(named: "thumbnail")
        self.playListThumbImageView.layer.masksToBounds = true;
        self.playListThumbImageView.layer.cornerRadius = 5.0
        
        guard let imageUrlString =  imageUrlString else{return}
        let url = URL(string: imageUrlString)
       // self.playListThumbImageView.kf.setImage(with: url)
        self.playListThumbImageView.kf.setImage(with: url, placeholder: UIImage(named: "placeHolderLandscape"), options:nil, progressBlock: nil, completionHandler: nil)
       // self.playListThumbImageView.downloadImageFrom(urlString: imageUrlString, imageMode: .scaleAspectFit)
        
//        let networkManager = NetworkingManager.sharedManager
//        networkManager.performAsyncGetRequest(imageUrlString) { (data, HTTPStatusCode, error) -> Void in
//
//            if HTTPStatusCode == 200 && error == nil {
//             self.playListThumbImageView.image = UIImage(data: data!)
//            }
//
//        }
//       // if image != nil{
//            //self.playListThumbImageView.image  = image
//       // }
    }
}

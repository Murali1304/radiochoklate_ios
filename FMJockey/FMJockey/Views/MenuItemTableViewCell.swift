//
//  MenuItemTableViewCell.swift
//  FMJockey
//
//  Created by iDev on 29/07/18.
//  Copyright © 2018 Radio Choklate. All rights reserved.
//

import UIKit

class MenuItemTableViewCell: UITableViewCell,UICollectionViewDataSource {
    var homeViewController:ViewController?
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return  7
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "menuCollectionCell", for: indexPath as IndexPath)
       // cell.backgroundColor = UIColor.red
        if let aView = cell.viewWithTag(11){
            (aView  as! UIImageView).image = UIImage(named: "logo104")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        homeViewController?.stationSelectedAtIndex(index: indexPath.item)
    }
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var moreItemLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setHomeViewController(landingViewController:ViewController){
        homeViewController = landingViewController
    }
    func setMenuItemCellContent(menuItem:VKSideMenuItem?,moreTitle:String?){
        if menuItem != nil{
            iconImageView.image = menuItem?.icon
            itemLabel.text = menuItem?.title
            moreItemLabel.isHidden = true
            iconImageView.isHidden = false
            itemLabel.isHidden = false
        }
        else if moreTitle != nil{
            iconImageView.isHidden = true
            itemLabel.isHidden = true
             moreItemLabel.isHidden = false
            moreItemLabel.text = moreTitle
        }
         iconImageView.image = iconImageView.image?.withRenderingMode(.alwaysTemplate)
        iconImageView.tintColor = UIColor(rgb: 0xFF8D00)
    
    }


}

extension MenuItemTableViewCell : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsPerRow:CGFloat = 4
        let hardCodedPadding:CGFloat = 13
       // let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
       // let itemHeight = 80.0//collectionView.bounds.height - (2 * hardCodedPadding)
        return CGSize(width: 70, height: 70.0)
    }
    
}

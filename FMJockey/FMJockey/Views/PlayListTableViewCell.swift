//
//  PlayListTableViewCell.swift
//  FMJockey
//
//  Created by MuraliKrishna on 15/07/18.
//  Copyright © 2018 Radio Choklate. All rights reserved.
//

import UIKit

class PlayListTableViewCell: UITableViewCell {
    @IBOutlet var thumbnailImageView:UIImageView!
     @IBOutlet var videoTitle:UILabel!
     @IBOutlet var videoDescription:UILabel!
    
    func setVideoCellContent(thumbUrlString:String?,videoTitle:String,description:String){
        self.videoTitle.text = videoTitle
        self.videoDescription.text = description
        self.thumbnailImageView.image = UIImage(named: "thumbnail")
      
        guard let imageUrlString =  thumbUrlString else{return}
    
        let url = URL(string: imageUrlString)
        self.thumbnailImageView.kf.setImage(with: url, placeholder: UIImage(named: "placeHolderLandscape"), options:nil, progressBlock: nil, completionHandler: nil)
        
       // self.thumbnailImageView.downloadImageFrom(urlString: imageUrlString, imageMode: .scaleAspectFit)
        
//        let networkManager = NetworkingManager.sharedManager
//        networkManager.performAsyncGetRequest(imageUrlString) { (data, HTTPStatusCode, error) -> Void in
//
//            if HTTPStatusCode == 200 && error == nil {
//                self.thumbnailImageView.image = UIImage(data: data!)
//            }
//
//        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

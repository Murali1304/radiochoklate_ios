//
//  AudioPlayTableViewCell.swift
//  FMJockey
//
//  Created by MuraliKrishna on 05/08/18.
//  Copyright © 2018 Radio Choklate. All rights reserved.
//

import UIKit

class AudioPlayTableViewCell: UITableViewCell {
 @IBOutlet var audioTitle:UILabel!
    
    @IBOutlet weak var audioPlayImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setVideoCellContent(videoTitle:String?){
        self.audioTitle.text = videoTitle
    }

}

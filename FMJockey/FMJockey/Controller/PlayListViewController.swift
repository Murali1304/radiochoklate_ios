//
//  PlayListViewController.swift
//  FMJockey
//
//  Created by MuraliKrishna on 15/07/18.
//  Copyright © 2018 Radio Choklate. All rights reserved.
//

import UIKit
import AVFoundation

class PlayListViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var noImagesFoundLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var prevAudioButton: UIButton!
    @IBOutlet weak var nextAudioButton: UIButton!
    @IBOutlet weak var playPauseAudioButton: UIButton!
    @IBOutlet weak var audioViewHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonVideos: UIButton!
    @IBOutlet weak var buttonAudios: UIButton!
    @IBOutlet weak var tableView:UITableView!
    var playListItem:PlaylistItem?
    let apiKey = "AIzaSyCfP_nVKf_a11OE841t9OkBbij6_jtV36Q"
    var arrayVideoIds = [String]()
    var categorieId = String()
    var arraySliderImageUrls = [String]()
    var currentPage:Int = 0
    var timer:Timer? = nil
    @IBOutlet weak var scrollView: UIScrollView!
    var videoItemsArray = [VideoItem]()
    var audioPlayerArray = [[String:AnyObject]]()
    var isAudioContent = false;
    var avPlayer:AVPlayer?
    var avPlayerItem:AVPlayerItem?
    private var playbackLikelyToKeepUpContext = 0
     @IBOutlet weak var playActivityIndicator: MaterialActivityIndicatorView!
    @IBOutlet weak var audioPlayView: UIView!
    @IBOutlet weak var playBackTimeSlider: UISlider!
    var avplayerTimeObserver:Any? = nil
    var currentAudioIndex = 0
    var areAudiosFetched = false
    @IBOutlet weak var TotalTime: UILabel!
    @IBOutlet weak var currentTime: UILabel!
    var  navTitle:String? = nil
    @IBOutlet weak var scrollViewActivityIndicator: UIActivityIndicatorView!
    var isPlayingNow:Bool = false
    var isAudioSliderTouchInside = false
    var isSeeking:Bool = false
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isAudioContent{
             return self.audioPlayerArray.count
        }
        return self.videoItemsArray.count
    }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isAudioContent{
            return 50
        }
        return 86
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         if isAudioContent{
                if (self.avPlayerItem != nil) {
                    self.avPlayerItem = nil
                }
                if (self.avPlayer != nil) {
                    self.avPlayer = nil
                }
                self.loadAudioForIndex(index: indexPath.row)
         } else{
                self.unloadAvPlayer();
                let playListViewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "YoutubeChannelViewController") as? YoutubeChannelViewController
                playListViewController?.currentPlayItemIndex = indexPath.row
                    playListViewController?.arrayVideoItems = self.videoItemsArray
                self.navigationController?.pushViewController(playListViewController!, animated: true)
            }
}
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         if isAudioContent{
             let  playListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AudioPlayTableViewCell", for: indexPath) as! AudioPlayTableViewCell
             playListTableViewCell.selectionStyle = .none
            let playListItem = self.audioPlayerArray[indexPath.row]
            playListTableViewCell.setVideoCellContent(videoTitle:playListItem["slug"] as? String)
            
            if currentAudioIndex < self.audioPlayerArray.count &&  currentAudioIndex == indexPath.row{
                 playListTableViewCell.audioPlayImage.isHidden = false;
                if !self.playActivityIndicator.isHidden ||  avPlayer?.rate == 0{
                    playListTableViewCell.audioPlayImage.stopSoundPlayImage()
                } else{
                playListTableViewCell.audioPlayImage.animateSoundPlayImage();
                }
            } else{
                playListTableViewCell.audioPlayImage.isHidden = true;
                playListTableViewCell.audioPlayImage.stopSoundPlayImage()
            }
               return playListTableViewCell
         } else{
       let  playListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PlayListTableViewCell", for: indexPath) as! PlayListTableViewCell
        let playListItem = self.videoItemsArray[indexPath.row]
            playListTableViewCell.selectionStyle = .none
       // playListTableViewCell.backgroundColor = UIColor.red
        playListTableViewCell.setVideoCellContent(thumbUrlString: playListItem.thumbnailUrl, videoTitle: playListItem.title!, description: playListItem.description!)
           
        return playListTableViewCell
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.noImagesFoundLabel.isHidden = true
        self.scrollViewActivityIndicator.isHidden = false
        let queue1 = DispatchQueue(label: "com.appcoda.sliderImages", qos: DispatchQoS.userInitiated)
        queue1.async {
            self.fetchSliderImages()
        }
         let queue2 = DispatchQueue(label: "com.appcoda.videos", qos: DispatchQoS.userInitiated)
        queue2.async {
              self.fetchVideos()
        }
        
        DispatchQueue.global(qos: .background).async{
            self.fetchAudioFiles()
        }
        
        super.setLeftNavigationBarItem(imageName: "back", titleName:self.navTitle!, buttonAction: #selector(super.backTapped))
        self.addGoHomeButtonToNavigationBar()
        
//        DispatchQueue.global(qos: .background).async{
//
//        }
        self.activityIndicatorView.isHidden = false
//        DispatchQueue.global(qos: .background).async{
//            self.fetchVideos()
//        }
        var prevImage = UIImage(named: "previousAudio")
        prevImage = prevImage?.withRenderingMode(.alwaysTemplate)
        self.prevAudioButton.tintColor = UIColor(rgb: 0xFF8D00)
        self.prevAudioButton .setImage(prevImage, for: .normal)
        
        var nextImage = UIImage(named: "nextAudio")
        nextImage = nextImage?.withRenderingMode(.alwaysTemplate)
        self.nextAudioButton.tintColor = UIColor(rgb: 0xFF8D00)
        self.nextAudioButton .setImage(nextImage, for: .normal)
        
        var playPauseImage = UIImage(named: "round")
        playPauseImage = playPauseImage?.withRenderingMode(.alwaysTemplate)
        self.playPauseAudioButton.tintColor = UIColor(rgb: 0xFF8D00)
        self.playPauseAudioButton .setImage(playPauseImage, for: .normal)
       
//        var audioImage = UIImage(named: "audio")
//        audioImage = audioImage?.withRenderingMode(.alwaysTemplate)
//        self.buttonAudios.tintColor = UIColor(rgb: 0xFF8D00)
//        self.buttonAudios.setImage(audioImage, for: .normal)
        
//        var videoImage = UIImage(named: "video")
//        videoImage = videoImage?.withRenderingMode(.alwaysTemplate)
//        self.buttonVideos.tintColor = UIColor(rgb: 0xFF8D00)
//        self.buttonVideos.setImage(videoImage, for: .normal)
//        self.buttonVideos.setTitleColor(UIColor(rgb: 0xFF8D00), for: .normal)
        
        self.audioVideoButtonConfig(isVideo: true)
        
        self.audioViewHeight.constant = 0.0
        self.audioPlayView.isHidden = true
        
        currentAudioIndex = 0
        self.playBackTimeSlider.setThumbImage(UIImage(named: "sliderRadius"), for: .normal)
        self.playBackTimeSlider.setThumbImage(UIImage(named: "sliderRadius"), for: .highlighted)
       self.playBackTimeSlider.tintColor = UIColor(rgb: 0xFF8D00)
        self.playBackTimeSlider.value = 0.0
        
        self.tableView.isHidden = true;
        
          areAudiosFetched = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.isSeeking = false
        self.addInterruptionNotification()
    }
    func setSliderPlayBackTime(){
        let duration : CMTime = avPlayerItem!.asset.duration
        let seconds : Float64 = CMTimeGetSeconds(duration)
        
        playBackTimeSlider!.maximumValue = Float(seconds)
        self.TotalTime.text = duration.durationText
        
        playBackTimeSlider!.isContinuous = false
        playBackTimeSlider!.tintColor = UIColor(rgb: 0xFF8D00)
        
        playBackTimeSlider?.addTarget(self, action: #selector(PlayListViewController.playbackSliderValueChanged(_:)), for: .valueChanged)
        
        self.playBackTimeSlider.addTarget(self, action: #selector(PlayListViewController.sliderTouchInside(_:)), for: .touchUpInside);
        self.playBackTimeSlider.addTarget(self, action: #selector(PlayListViewController.sliderTouchOutside(_:)), for: .touchUpOutside);
        
      avplayerTimeObserver =   avPlayer!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, 1), queue: DispatchQueue.main) { (CMTime) -> Void in
            if self.avPlayer?.currentItem?.status == .readyToPlay {
                let time : Float64 = CMTimeGetSeconds((self.avPlayer?.currentTime())!);
                self.currentTime.text =  (self.avPlayer?.currentTime())!.durationText
               
                if !self.isAudioSliderTouchInside{
                    self.playBackTimeSlider?.value = Float ( time );
                }
               
            }
        }
    }
    @objc func sliderTouchInside(_ playbackSlider:UISlider){
        self.isAudioSliderTouchInside = true
    }
    @objc func sliderTouchOutside(_ playbackSlider:UISlider){
         self.isAudioSliderTouchInside = false
    }
    @objc func playbackSliderValueChanged(_ playbackSlider:UISlider)
    {
        
        self.isSeeking = true
        let seconds : Int64 = Int64((playBackTimeSlider?.value)!)
        let targetTime:CMTime = CMTimeMake(seconds, 1)
        //let targetTime:CMTime = CMTimeMakeWithSeconds(77.000000, Int32(seconds))
        self.currentTime.text =  targetTime.durationText
        avPlayer!.seek(to: targetTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
        
//        if avPlayer!.rate == 0
//        {
//            //CMTime time = CMTimeMakeWithSeconds(77.000000, timeScale);
//            //[self.player seekToTime:time toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
//
////            self.playPauseAudioButton.isUserInteractionEnabled = true;
////            self.playActivityIndicator.isHidden = true;
////            self.playActivityIndicator.stopAnimating();
////            self.playPauseButtonClicked(nil)
//            //avPlayer?.play()
//       }
    }
    func fetchVideos(){
        for videoId in self.arrayVideoIds{
            let videoInfo =  "https://www.googleapis.com/youtube/v3/videos?part=snippet&id=\(videoId)&key=\(self.apiKey)"
            self.performGetRequestFor(urlString: videoInfo, completionHandler: { dictResponse in
                guard let dictResponse = dictResponse else{return}
                let arrayItems = dictResponse["items"] as! [[String:AnyObject]]
                if arrayItems.count == 0 {return}
                let snippet = arrayItems[0]["snippet"] as! [String:AnyObject]
                let localizedContent = snippet["localized"] as! [String:String]
                let thumbNials = snippet["thumbnails"] as! [String:AnyObject]
                let vidoItem = VideoItem(videoId: videoId, thumbnailUrl: (thumbNials["default"] as! [String:AnyObject])["url"] as? String, title: localizedContent["title"] , description:localizedContent["description"])
                self.videoItemsArray.append(vidoItem)
                if(videoId == self.arrayVideoIds.last){
                    print(self.videoItemsArray.debugDescription)
                    self.activityIndicatorView.isHidden = true
                    self.tableView .reloadData()
                     self.tableView.isHidden = false;
                }
            })
        }
    }
    func fetchAudioFiles(){
        let networkManager = NetworkingManager.sharedManager
        let sliderImages = "/media?categories=\(categorieId)&mime_type=audio/mpeg&per_page=40&page=1"
        
        let strHomePageImagesUrl =  URLConstants.baseUrl+sliderImages
        
        networkManager.performAsyncGetRequest(strHomePageImagesUrl) { (data, HTTPStatusCode, error) -> Void in
         self.areAudiosFetched = true
            self.activityIndicatorView.isHidden = true
            if HTTPStatusCode == 200 && error == nil {
                do{
                    let resultsDict = try JSONSerialization.jsonObject(with: data!, options: []) as! [[String:AnyObject]]
                    //print("Result Dict \(resultsDict.debugDescription)")
                    for dictAudioInfo in resultsDict{
                        var dictTemp = [String:AnyObject]()
                        let imageUrl = dictAudioInfo["guid"]!["rendered"] as! String
                        dictTemp["audioUrl"] = imageUrl as AnyObject
                        dictTemp["slug"] = dictAudioInfo["slug"]
                        self.audioPlayerArray.append(dictTemp)
                    }
                   
                     self.tableView.isHidden = false;
                    if self.isAudioContent == true{
                        self.buttonAudioAction(nil)
                    } else{
                        //self.tableView.reloadData()
                    }
                    //for dictT
                    ///guard let guid = resultsDict[""]
                   // self .parseImageUrls(response: resultsDict)
                } catch {
                    print(error)
                }
            }
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &playbackLikelyToKeepUpContext {
            if (avPlayer?.currentItem!.isPlaybackLikelyToKeepUp)! {
                if  self.isSeeking {
                    self.isSeeking = false
                    return
                }
                // loadingIndicatorView.stopAnimating() or something else
                self.playPauseAudioButton.isUserInteractionEnabled = true;
                self.playActivityIndicator.isHidden = true;
                self.playActivityIndicator.stopAnimating();
                 self.playBackTimeSlider?.isUserInteractionEnabled = true;
                self.playPauseButtonClicked(nil)
                self.tableView.reloadData()
            } else if (avPlayer?.currentItem?.isPlaybackBufferEmpty)!{
                self.displayConnectionErrorAlertView()
            }
            
            else {
                // loadingIndicatorView.startAnimating() or something else
                //self.playPauseButton.isUserInteractionEnabled = false;

            }
        }
    }
    func fetchSliderImages(){
        let networkManager = NetworkingManager.sharedManager
         let sliderImages = "/media?categories=\(categorieId)&mime_type=image/jpeg"
        
        let strHomePageImagesUrl =  URLConstants.baseUrl+sliderImages
        
        networkManager.performGetRequest(strHomePageImagesUrl) { (data, HTTPStatusCode, error) -> Void in
            if HTTPStatusCode == 200 && error == nil {
                do{
                    let resultsDict = try JSONSerialization.jsonObject(with: data!, options: []) as! [[String:AnyObject]]
                    //print("Result Dict \(resultsDict.debugDescription)")
                    self .parseImageUrls(response: resultsDict)
                } catch {
                    print(error)
                }
            } else{
                super.displayConnectionErrorAlertView();
                self.activityIndicatorView.isHidden = true
                self.scrollViewActivityIndicator.isHidden = true
            }
        }
    }
    
    func parseImageUrls(response:[[String:AnyObject]]) {
        //self.scrollView.contentSize = CGSize(width: response.count*375, height: self.view.frame.size.height)
        for dictItem in response{
            guard let guid = dictItem["guid"]else {return}
            guard let renderedImageUrl = guid["rendered"] else {return}
            self.arraySliderImageUrls.append(renderedImageUrl as! String)
        }
        if self.arraySliderImageUrls.count == 0{
            self.noImagesFoundLabel.isHidden  = false
        }
          self.addScrollViewContentImages()
        
    }
    func addScrollViewContentImages() {
        var lastView:UIView?
        
        let width = self.view.frame.size.width
        let position = 0
        let numberOfImages = self.arraySliderImageUrls.count
        let height = 160
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 0)
        for index in 0..<numberOfImages {
            
            let view = UIImageView()
            /// view.contentMode = .scaleAspectFit
            let url = URL(string: self.arraySliderImageUrls[index])
            view.kf.setImage(with: url, placeholder: UIImage(named: "placeHolderLandscape"), options:nil, progressBlock: nil, completionHandler: nil)
            
            scrollView.addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
            scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(position)-[view(\(height))]-\(position)-|", options: [], metrics: nil, views: ["view": view]))
            
            //If view is first then pin the leading edge to main ScrollView otherwise to the last View.
            if lastView == nil && index == 0 {
                scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view(\(width))]", options: [], metrics: nil, views: ["view": view]))
            } else {
                scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[lastView]-0-[view(\(width))]", options: [], metrics: nil, views: ["lastView": lastView!, "view": view]))
            }
            
            //If View is last then pin the trailing edge to mainScrollView trailing edge.
            if index == numberOfImages-1 {
                scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[view]-0-|", options: [], metrics: nil, views: ["view": view]))
            }
            
            //Assign the current View as last view to keep the reference for next View.
            lastView = view
            
        }
        
        self.scrollViewActivityIndicator.isHidden = true
        
        self.updateBannerImage(nil)
        self.addGestureRecognizersToScrollview()
        self.timer =  Timer.scheduledTimer(timeInterval: 3.0,
                             target: self,
                             selector: #selector(self.updateBannerImage(_:)),
                             userInfo: nil,
                             repeats: true)
        //self.isScrollviewLoaded = true
        
        
        
    }
    
    func addGestureRecognizersToScrollview(){
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        swipeRight.delegate = self
        self.scrollView.addGestureRecognizer(swipeRight)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture(_:)))
        swipeDown.direction = UISwipeGestureRecognizerDirection.left
        swipeDown.delegate = self
        self.scrollView.addGestureRecognizer(swipeDown)
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
                self.updateBannerImagesOnSwipe(isRightSwipe: true)
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
                self.updateBannerImagesOnSwipe(isRightSwipe: false)
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool{
        return true
    }
    func updateBannerImagesOnSwipe(isRightSwipe:Bool){
        let numberOfPages = (Int)(self.scrollView.contentSize.width/scrollView.frame.size.width)
        if (!isRightSwipe){
            if currentPage < numberOfPages {
                 currentPage = currentPage + 1
                if currentPage == numberOfPages{
                    currentPage = currentPage - 1
                    return
                }
                let newX = CGFloat(currentPage) * scrollView.frame.size.width
                scrollView.setContentOffset(CGPoint(x: newX, y: 0), animated: true)
            }
        } else{
            if(currentPage>0){
            
                if currentPage > 0 {
                    currentPage = currentPage - 1
                     let newX = CGFloat(currentPage) * scrollView.frame.width
                    scrollView.setContentOffset(CGPoint(x: newX, y: 0), animated: true)
                }
            }
        }
    }
    
    @objc func updateBannerImage(_ timer: Timer?) {
        let numberOfPages = (Int)(self.scrollView.contentSize.width/scrollView.frame.size.width)
        // let maxX = scrollView.contentSize.width - scrollView.frame.width+100
       // let newX = CGFloat(currentPage) * scrollView.frame.width
        if currentPage < numberOfPages {
            currentPage = currentPage + 1
            if currentPage == numberOfPages{
                currentPage = -1
                return
            }
            let newX = CGFloat(currentPage) * scrollView.frame.size.width
            scrollView.setContentOffset(CGPoint(x: newX, y: 0), animated: true)
        } else{
            currentPage = 0
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.timer?.invalidate()
        self.timer = nil
    }
    
    
    
    
    
    
    
    
    
    
    
    
    func performGetRequestFor(urlString:String, completionHandler:@escaping (_ dictResponse:[String:AnyObject]?) -> Void ){
        let networkManager = NetworkingManager.sharedManager
        
        networkManager.performAsyncGetRequest(urlString, completion: { (data, HTTPStatusCode, error) -> Void in
            
            if HTTPStatusCode == 200 && error == nil {
                do{
                    let resultsDict = try JSONSerialization.jsonObject(with: data!, options: []) as! [String:AnyObject]
                    //print("Result Dict \(resultsDict.debugDescription)")
                    completionHandler(resultsDict)
                } catch {
                    print(error)
                }
                
            } else{
                super.displayConnectionErrorAlertView();
                self.activityIndicatorView.isHidden = true
            }
        })
    }
    func fetchVideos(playListItem:PlaylistItem){
        
          let videosList = "https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails&playlistId=\(playListItem.playListId)&key=\(apiKey)&maxResults=50"
        
        self.performGetRequestFor(urlString: videosList) { dictResponse in
             guard let dictResponse = dictResponse else{return}
             let aVideoItemsArray = dictResponse["items"] as! [[String:AnyObject]]
            var arrayVideoIds  = [String]()
                 for videoItem in aVideoItemsArray{
                     let contentDetails = videoItem["contentDetails"] as! [String:AnyObject]
                     let videoId = contentDetails["videoId"] as! String
                     arrayVideoIds.append(videoId)
                 }
            let queue = DispatchQueue(label: "com.fmjockey.queue")
            queue.sync {
                for videoId in arrayVideoIds{
                    let videoInfo =  "https://www.googleapis.com/youtube/v3/videos?part=snippet&id=\(videoId)&key=\(self.apiKey)"
                    self.performGetRequestFor(urlString: videoInfo, completionHandler: { dictResponse in
                       guard let dictResponse = dictResponse else{return}
                        let arrayItems = dictResponse["items"] as! [[String:AnyObject]]
                        if arrayItems.count == 0 {return}
                        let snippet = arrayItems[0]["snippet"] as! [String:AnyObject]
                        let localizedContent = snippet["localized"] as! [String:String]
                        let thumbNials = snippet["thumbnails"] as! [String:AnyObject]
                        let vidoItem = VideoItem(videoId: videoId, thumbnailUrl: (thumbNials["default"] as! [String:AnyObject])["url"] as? String, title: localizedContent["title"] , description:localizedContent["description"])
                        self.videoItemsArray.append(vidoItem)
                        if(videoId == arrayVideoIds.last){
                            //print(self.videoItemsArray.debugDescription)
                            self.tableView .reloadData()
                             self.tableView.isHidden = false;
                        }
                    })
                }
            }
            
         }

       
    }
    
    func audioVideoButtonConfig(isVideo:Bool){
        
        if isVideo {
            var videoImage = UIImage(named: "video")
            videoImage = videoImage?.withRenderingMode(.alwaysTemplate)
            self.buttonVideos.tintColor = UIColor(rgb: 0xFF8D00)
            self.buttonVideos.setImage(videoImage, for: .normal)
            self.buttonVideos.setTitleColor(UIColor(rgb: 0xFF8D00), for: .normal)
            
            self.buttonAudios.tintColor = UIColor.darkGray
            self.buttonAudios.setTitleColor(UIColor.darkGray, for: .normal)
            
        } else{
            var audioImage = UIImage(named: "audio")
            audioImage = audioImage?.withRenderingMode(.alwaysTemplate)
            self.buttonAudios.tintColor = UIColor(rgb: 0xFF8D00)
            self.buttonAudios.setImage(audioImage, for: .normal)
            self.buttonAudios.setTitleColor(UIColor(rgb: 0xFF8D00), for: .normal)
            
            self.buttonVideos.tintColor = UIColor.darkGray
            self.buttonVideos.setTitleColor(UIColor.darkGray, for: .normal)
        }
        
    }
    
    @IBAction func buttonVideoClicked(_ sender: Any) {
        
        
        audioViewHeight.constant = 0
        self.view.updateConstraintsIfNeeded()
        self.audioPlayView.isHidden = true
        
        self.isAudioContent = false
        self.tableView.reloadData()
         self.audioVideoButtonConfig(isVideo: true)
    }
    @IBAction func buttonAudioAction(_ sender: Any?) {
        self.isAudioContent = true
        if self.areAudiosFetched == true{
            self.audioVideoButtonConfig(isVideo: false)
            
            self.tableView.reloadData()
            if self.audioPlayerArray.count == 0{
                self.audioPlayView.isHidden = true
                audioViewHeight.constant = 0
                self.view.updateConstraintsIfNeeded()
            } else{
                self.audioPlayView.isHidden = false
                audioViewHeight.constant = 80
                self.view.updateConstraintsIfNeeded()
                 self.loadAudioForIndex(index: 0)
            }
           
        } else{
         self.tableView.isHidden = false
            self.activityIndicatorView.isHidden = false
        }
        

       

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         self.removeInterruptionNotification()
        self.unloadAvPlayer();
        
       
    }
    
    func unloadAvPlayer(){
        self.avPlayer?.removeObserver(self, forKeyPath: "currentItem.playbackLikelyToKeepUp", context: &self.playbackLikelyToKeepUpContext)
        
        //self.avPlayer?.removeObserver(self, forKeyPath: "playbackBufferEmpty", context: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: avPlayerItem)
        
        if self.avPlayerItem != nil {
            self.avPlayerItem = nil
        }
        if self.avPlayer  != nil {
            //self.avPlayer?.pause()
            self.avPlayer = nil
        }
    }
    func loadAudioForIndex(index:Int){
        currentAudioIndex = index
        if self.audioPlayerArray.count == 0{
            return
        }
        if index == 0 && self.audioPlayerArray.count == 1{
            self.nextAudioButton.isEnabled = false
            self.prevAudioButton.isEnabled = false
        } else if index == self.audioPlayerArray.count-1{
            self.prevAudioButton.isEnabled = true
            self.nextAudioButton.isEnabled = false
        } else if index == 0{
            self.prevAudioButton.isEnabled = false
            self.nextAudioButton.isEnabled = true
        } else{
            self.nextAudioButton.isEnabled = true
            self.prevAudioButton.isEnabled = true
        }
        guard  let url = NSURL(string: self.audioPlayerArray[index]["audioUrl"] as! String) else {return}
        
        self.avPlayer?.removeObserver(self, forKeyPath: "currentItem.playbackLikelyToKeepUp", context: &self.playbackLikelyToKeepUpContext)
         //self.avPlayer?.removeObserver(self, forKeyPath: "playbackBufferEmpty", context: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: avPlayerItem)
        
        self.avPlayerItem = AVPlayerItem.init(url: url as URL)
        self.avPlayer = AVPlayer.init(playerItem: self.avPlayerItem)
        self.avPlayer?.volume = 1.0
        self.playActivityIndicator.color = UIColor.white
        self.playActivityIndicator.isHidden = false;
        self.playActivityIndicator.startAnimating();
        
        var playPauseImage = UIImage(named: "round")
        playPauseImage = playPauseImage?.withRenderingMode(.alwaysTemplate)
        self.playPauseAudioButton.tintColor = UIColor(rgb: 0xFF8D00)
        self.playPauseAudioButton.setImage(playPauseImage, for: .normal)
        
        self.setSliderPlayBackTime()
        self.avPlayer?.addObserver(self, forKeyPath: "currentItem.playbackLikelyToKeepUp",
                                   options: .new, context: &self.playbackLikelyToKeepUpContext)
        
        //self.avPlayer?.currentItem?.addObserver(self, forKeyPath: "playbackBufferEmpty", options: .new, context: nil)
        
     //    NotificationCenter.default.addObserver(self, selector: #selector(self.audioDidFinishPlaying(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: avPlayerItem)
        
        //playerItem addObserver:self forKeyPath:@"playbackBufferEmpty"   options:NSKeyValueObservingOptionNew context:nil];
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: .mixWithOthers)
            print("Playback OK")
            try AVAudioSession.sharedInstance().setActive(true)
            print("Session is Active")
        } catch {
            print(error)
        }
        self.playBackTimeSlider?.value = 0.0
        self.playBackTimeSlider?.isUserInteractionEnabled = false;
        self.tableView.reloadData()
    }
    
    @objc func audioDidFinishPlaying(_ note: NSNotification) {
        print("call")
        self.nextButtonClicked(nil)
    }
    @IBAction func prevButtonClicked(_ sender: Any) {
        if currentAudioIndex > 0 {
            currentAudioIndex =  currentAudioIndex - 1
            self.loadAudioForIndex(index: currentAudioIndex)
            self.nextAudioButton.isEnabled = true
        } else{
            self.prevAudioButton.isEnabled = false
        }
    }
    @IBAction func nextButtonClicked(_ sender: Any?) {
        
        if currentAudioIndex < self.audioPlayerArray.count-1 {
            currentAudioIndex =  currentAudioIndex + 1
            self.loadAudioForIndex(index: currentAudioIndex)
            self.prevAudioButton.isEnabled = true
            
        } else{
            self.nextAudioButton.isEnabled = false
        }
    }
    @IBAction func playPauseButtonClicked(_ sender: Any?) {
        
        var playPauseImage:UIImage? = nil
        if avPlayer?.rate != 0 && avPlayer?.error == nil{
            avPlayer?.pause()
            playPauseImage = UIImage(named: "circledPlay")
            isPlayingNow = false
//            self.playEquilizer.image = UIImage(named: "pauseEquilizer")
//            self.playEquilizer.image = self.playEquilizer.image?.withRenderingMode(.alwaysTemplate)
//            playEquilizer.tintColor = UIColor(rgb: 0xFF8D00)
        } else{
            avPlayer?.play()
            playPauseImage = UIImage(named: "circledPause")
            isPlayingNow = true
//            self.playEquilizer.image = UIImage.gifImageWithName("playEquilizer")
//            self.playEquilizer.image = self.playEquilizer.image?.withRenderingMode(.alwaysTemplate)
//            playEquilizer.tintColor = UIColor(rgb: 0xFF8D00)
        }
        self.tableView.reloadData()
        playPauseImage = playPauseImage?.withRenderingMode(.alwaysTemplate)
        self.playPauseAudioButton.tintColor = UIColor(rgb: 0xFF8D00)
        self.playPauseAudioButton .setImage(playPauseImage, for: .normal)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addInterruptionNotification() {
        // add audio session interruption notification
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.handleInterruption(_:)),
                                               name: NSNotification.Name.AVAudioSessionInterruption,
                                               object: nil)
    }
    
    func removeInterruptionNotification(){
         NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVAudioSessionInterruption, object: nil)
    }
    
    @objc func handleInterruption(_ notification: NSNotification){
        if notification.name != NSNotification.Name.AVAudioSessionInterruption
            || notification.userInfo == nil{
            return
        }
        var info = notification.userInfo!
        var intValue: UInt = 0
        (info[AVAudioSessionInterruptionTypeKey] as! NSValue).getValue(&intValue)
        if let type = AVAudioSessionInterruptionType(rawValue: intValue) {
            switch type {
            case .began:
                // interruption began
                print("began")
            case .ended:
                if isPlayingNow {
                    self.playPauseButtonClicked(nil)
                }
                // interruption ended
                print("ended")
                
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVAudioSessionInterruption, object: nil)
        self.timer?.invalidate()
        self.timer = nil
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CMTime {
    var durationText:String {
        let totalSeconds = CMTimeGetSeconds(self)
        let hours:Int = Int(totalSeconds / 3600)
        let minutes:Int = Int(totalSeconds.truncatingRemainder(dividingBy: 3600) / 60)
        let seconds:Int = Int(totalSeconds.truncatingRemainder(dividingBy: 60))
        
        if hours > 0 {
            return String(format: "%i:%02i:%02i", hours, minutes, seconds)
        } else {
            return String(format: "%02i:%02i", minutes, seconds)
        }
    }
}

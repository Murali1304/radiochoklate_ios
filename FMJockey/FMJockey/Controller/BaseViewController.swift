//
//  BaseViewController.swift
//  FMJockey
//
//  Created by MuraliKrishna on 08/07/18.
//  Copyright © 2018 Radio Choklate. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController,VKSideMenuDataSource,VKSideMenuDelegate {
    var menuLeft:VKSideMenu = VKSideMenu()
    var arrayMenuItems  = [VKSideMenuItem]()
    
    func setLeftNavigationBarItem(imageName:String,titleName:String,buttonAction:Selector) {
        
        let customView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 100.0, height: 44.0))
        customView.backgroundColor = UIColor.clear
        
        let button = UIButton.init(type: .custom)
        var image = UIImage(named: imageName)
        image = image?.withRenderingMode(.alwaysTemplate)
        //button.setBackgroundImage(image, for: .normal)
         button.setImage(image, for: .normal)
        button.tintColor = UIColor.white
        
        button.frame = CGRect(x: 0.0, y: 2.0, width: 35.0, height: 40.0)
        button.addTarget(self, action: buttonAction, for: .touchUpInside)
        customView.addSubview(button)
        
        let marginX = CGFloat(button.frame.origin.x + button.frame.size.width + 5)
        let label = UILabel(frame: CGRect(x: marginX+10, y: 2.0, width: 200.0, height: 40.0))
        label.text = titleName
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.left
        label.backgroundColor = UIColor.clear
        customView.addSubview(label)
        
        let leftButton = UIBarButtonItem(customView: customView)
        self.navigationItem.leftBarButtonItem = leftButton
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.barTintColor =  UIColor(rgb: 0xFF8D00)
    }
    func setSlideMenu(){
        self.menuLeft = VKSideMenu(size: 290, andDirection: .fromLeft)
        self.menuLeft.dataSource = self
        self.menuLeft.delegate = self
        //self.menuLeft.addSwipeGestureRecognition(self.view)
        
        let vkSideMenuItem = VKSideMenuItem()
        vkSideMenuItem.title = "Radio OM"
        vkSideMenuItem.icon = UIImage(named:"radio_Icon")
        self.arrayMenuItems = self.arrayMenuItemsForValues(arrayInput: [["title":"","imageName":""],["title":"Radio Bhakti","imageName":"radio_Icon"],["title":"Stories","imageName":"heart_Icon"],["title":"Rangamancha","imageName":"radio_Icon"],["title":"Talk Shows","imageName":"talk_Icon"],["title":"Comedy Tadka","imageName":"radio_Icon"],["title":"Love Shows","imageName":"heart_Icon"],["title":"New Chocklet Songs","imageName":"music_Icon"]])
    }
    func arrayMenuItemsForValues(arrayInput:[[String:String]]) -> [VKSideMenuItem] {
        var arrayItems = [VKSideMenuItem]()
        for dict in arrayInput{
            let vkSideMenuItem = VKSideMenuItem()
            vkSideMenuItem.title = dict["title"]
            if (dict["imageName"] != nil) {
                vkSideMenuItem.icon = UIImage(named:dict["imageName"]!)
            }
            arrayItems.append(vkSideMenuItem)
        }
        return arrayItems
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sideMenu(_ sideMenu: VKSideMenu!, didSelectRowAt indexPath: IndexPath!) {
        let sildeMenuItem = self.arrayMenuItems[indexPath.row]
        if sildeMenuItem.title == "Stories"{
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoveStoriesViewController") as? LoveStoriesViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
   /* -(void)sideMenu:(VKSideMenu *)sideMenu didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    }*/
    func numberOfSections(in sideMenu: VKSideMenu!) -> Int {
        return 1
    }
    
    func sideMenu(_ sideMenu: VKSideMenu!, numberOfRowsInSection section: Int) -> Int {
        return self.arrayMenuItems.count
    }
    
    func sideMenu(_ sideMenu: VKSideMenu!, itemForRowAt indexPath: IndexPath!) -> VKSideMenuItem! {
        return self.arrayMenuItems[indexPath.row]
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0xFF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0xFF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    @objc func menuTapped() {
        self.menuLeft.show()
        
    }
    @objc func backTapped() {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    func animateImageView(imageView:UIImageView){
        
    }
    
    func displayConnectionErrorAlertView(){
        let alertViewController = UIAlertController.init(title: "FM Jockey", message: "Connection error occured.", preferredStyle: .alert)
        let actionOK = UIAlertAction(title: "OK", style: .default, handler: { (alertAction) in
            
        })
        alertViewController.addAction(actionOK)
        self.present(alertViewController, animated: true, completion: {
            
        })
    }
    
    func addGoHomeButtonToNavigationBar(){
        let button = UIButton.init(type: .custom)
        
        var image = UIImage(named: "golive")
        image = image?.withRenderingMode(.alwaysTemplate)
        
        //set image for button
        button.setImage(image, for: UIControlState.normal)
        //add function for button
        button.addTarget(self, action: #selector(BaseViewController.goHomePressed), for:.touchUpInside)
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 93, height: 51)
        button.tintColor = UIColor.white
        button.blink()
        
        let barButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    @objc func goHomePressed(){
      let viewControllerStack  = self.navigationController?.viewControllers
        var homeViewController:ViewController?
        for aViewController in viewControllerStack! {
            if aViewController is ViewController{
                homeViewController = aViewController as? ViewController
                break
            }
        }
        guard let homeViewControllerr = homeViewController else {return}
        self.navigationController?.popToViewController(homeViewControllerr, animated: true)
        print("goHomePressed")
    }
    
    func stationSelectedAtIndex(index:Int){
        if index == 1{
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoveStoriesViewController") as? LoveStoriesViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        
    }
}

extension UIImageView{
    func stopSoundPlayImage(){
        self.stopAnimating()
        self.image = UIImage(named: "14")
    }
    func animateSoundPlayImage(){
        
        let imgListArray :NSMutableArray = []
        for countValue in 1...12
        {
            
            let strImageName : String = "\(countValue).png"
            var image  = UIImage(named:strImageName)
            image = image?.withRenderingMode(.alwaysTemplate)
            imgListArray.add(image)
        }
        
        self.animationImages = imgListArray as? [UIImage];
        self.animationDuration = 1.0
        self.startAnimating()
    }
}
extension UIButton {
    open override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        return self.bounds.contains(point) ? self : nil
    }
    func blink(enabled: Bool = true, duration: CFTimeInterval = 1.0, stopAfter: CFTimeInterval = 0.0 ) {
        enabled ? (UIView.animate(withDuration: duration, //Time duration you want,
            delay: 0.0,
            options: [.curveEaseInOut, .autoreverse, .repeat],
            animations: { [weak self] in self?.alpha = 0.0 },
            completion: { [weak self] _ in self?.alpha = 1.0 })) : self.layer.removeAllAnimations()
        if !stopAfter.isEqual(to: 0.0) && enabled {
            DispatchQueue.main.asyncAfter(deadline: .now() + stopAfter) { [weak self] in
                self?.layer.removeAllAnimations()
            }
        }
    }
}

//
//  SplashViewController.swift
//  FMJockey
//
//  Created by MuraliKrishna on 02/08/18.
//  Copyright © 2018 Radio Choklate. All rights reserved.
//

import UIKit

class SplashViewController: BaseViewController {
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    var arraySliderImageUrls = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         //MBProgressHUD.showAdded(to: self.view, animated: true)
        //loadingActivityIndicator
      // let  activityIndicator = activityIndicator
        self.navigationController?.navigationBar.isHidden = true
        DispatchQueue.global(qos: .default).async{
            self.getLandingPageInfo();
        }
        
    }

    func getLandingPageInfo(){
        let networkManager = NetworkingManager.sharedManager
        let strHomePageImagesUrl =  URLConstants.baseUrl+URLConstants.homePageImages
        
        networkManager.performAsyncGetRequest(strHomePageImagesUrl) { (data, HTTPStatusCode, error) -> Void in
            if HTTPStatusCode == 200 && error == nil {
                do{
                    let resultsDict = try JSONSerialization.jsonObject(with: data!, options: []) as! [[String:AnyObject]]
                    print("Result Dict \(resultsDict.debugDescription)")
                    self .parseImageUrls(response: resultsDict)
                } catch {
                    super.displayConnectionErrorAlertView();
                    self.loadingActivityIndicator.isHidden = true
                    print(error)
                }
            } else{
                super.displayConnectionErrorAlertView();
                 self.loadingActivityIndicator.isHidden = true
            }
        }
    }
    
    func parseImageUrls(response:[[String:AnyObject]]) {
        //self.scrollView.contentSize = CGSize(width: response.count*375, height: self.view.frame.size.height)
        for dictItem in response{
            guard let guid = dictItem["guid"]else {return}
            guard let renderedImageUrl = guid["rendered"] else {return}
            self.arraySliderImageUrls.append(renderedImageUrl as! String)
            
            let view = UIImageView()
            let url = URL(string: renderedImageUrl as! String)
            view.kf.setImage(with: url, placeholder: UIImage(named: "placeHolderLandscape"), options:nil, progressBlock: nil, completionHandler: nil)
            //view.downloadImageFrom(urlString: renderedImageUrl as! String, imageMode: .scaleAspectFit)
        }
        //MBProgressHUD.hide(for: self.view, animated: true)
        let viewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewController") as? ViewController
        viewController?.arraySliderImageUrls = self.arraySliderImageUrls
        self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.pushViewController(viewController!, animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

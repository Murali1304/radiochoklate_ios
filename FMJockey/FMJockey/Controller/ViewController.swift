//
//  ViewController.swift
//  FMJockey
//
//  Created by Sahoo, subhransu on 7/8/18.
//  Copyright © 2018 Radio Choklate. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate {
    @IBOutlet weak var liveAudioView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var volumeImage: UIImageView!
    @IBOutlet weak var radioChkOrignalsLabel: UILabel!
    @IBOutlet weak var playPauseButton:UIButton!
    @IBOutlet weak var playEquilizer: UIImageView!
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var volumeSlider: UISlider!
    @IBOutlet weak var videoContentTableView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    private var playbackLikelyToKeepUpContext = 0
    @IBOutlet weak var playActivityIndicator: MaterialActivityIndicatorView!
    var isPlayingNow :Bool = false;
    var isPlayDisAppear :Bool = false;
    var isScrollviewLoaded = false
    
     var currentPage:Int = 0
    var avPlayer:AVPlayer?
    var avPlayerItem:AVPlayerItem?
    var arraySliderImageUrls = [String]()
    var arrayHomePageMenuItems = [VKSideMenuItem]()
    var arrayVideoItems = [VideoItem]()
    
    @IBAction func volumeSliderChanged(_ sender: UISlider) {
        self.avPlayer?.volume = sender.value
        if (self.avPlayer?.volume)! > 0.0 as Float{
            self.volumeImage.image = UIImage(named: "volume")
        } else{
            self.volumeImage.image = UIImage(named: "mute")
        }

        self.volumeImage.image = self.volumeImage.image?.withRenderingMode(.alwaysTemplate)
        self.volumeImage.tintColor = UIColor(rgb: 0xFF8D00)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.menuTableView{
            return 70
            
        } else {
            return 86
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.menuTableView{
            return 1
        }
        return self.arrayVideoItems.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if tableView == self.menuTableView{
         } else{
            let playListViewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "YoutubeChannelViewController") as? YoutubeChannelViewController
            playListViewController?.currentPlayItemIndex = indexPath.row
            playListViewController?.arrayVideoItems = self.arrayVideoItems
            self.navigationController?.pushViewController(playListViewController!, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if tableView == self.menuTableView {
        let  homePageMenuItemTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MenuItemTableViewCell", for: indexPath) as! MenuItemTableViewCell
            homePageMenuItemTableViewCell.setHomeViewController(landingViewController: self)
//        if indexPath.row < self.arrayHomePageMenuItems.count{
//         let homePageMenuItem = self.arrayHomePageMenuItems[indexPath.row] as VKSideMenuItem
//                homePageMenuItemTableViewCell.setMenuItemCellContent(menuItem: homePageMenuItem, moreTitle: nil)
//           } else {
//             homePageMenuItemTableViewCell.setMenuItemCellContent(menuItem: nil, moreTitle: "All Live Radio Stations")
//          }
            homePageMenuItemTableViewCell.selectionStyle = .none
            return homePageMenuItemTableViewCell
    
        } else {
            let  playListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PlayListTableViewCell", for: indexPath) as! PlayListTableViewCell
           let playListItem = self.arrayVideoItems[indexPath.row]
           playListTableViewCell.setVideoCellContent(thumbUrlString: playListItem.thumbnailUrl, videoTitle: playListItem.title!, description: playListItem.description!)
            playListTableViewCell.selectionStyle = .none
            return playListTableViewCell
        }
        
    }
   
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if self.isScrollviewLoaded {
            self.isScrollviewLoaded = false
            self.radioChkOrignalsLabel.isHidden = false
            self.createAndLoadHomePageMenu()
        }
    }
    @IBAction func playPauseRadioClicked(_ sender: Any?) {
        var playPauseImage:UIImage? = nil
        if avPlayer?.rate != 0 && avPlayer?.error == nil{
            avPlayer?.pause()
             playPauseImage = UIImage(named: "circledPlay")
            self.playEquilizer.stopSoundPlayImage()
            isPlayingNow = false
        } else{
             playPauseImage = UIImage(named: "circledPause")
            
            self.playEquilizer.animateSoundPlayImage();
            self.playEquilizer.tintColor = UIColor(rgb: 0xFF8D00)
             isPlayingNow = true
//            let  time = getLiveDuration()
//            if   time > 0.0 {
//             let targetTime:CMTime = CMTimeMake(Int64(time), 1)
//                 avPlayer?.seek(to: targetTime)
//            }
            
           // avPlayerItem = AVPlayerItem(url: NSURL(string:"http://192.64.80.222:5555/")! as URL)
           // avPlayer?.replaceCurrentItem(with: avPlayerItem)
            
           // avPlayer?.seek(to: kCMTimePositiveInfinity)
//            avPlayer?.currentItem?.seek(to: kCMTimePositiveInfinity, completionHandler: { (isSeeked) in
//                print("seek")
//                self.avPlayer?.play()
//            })
            self.avPlayer?.play()
           //
            
//            let duration = self.getLiveDuration()
//            if duration  == 0.0{
//
//                avPlayer?.seek(to: kCMTimePositiveInfinity)
//                avPlayer?.play()
//            } else{
//                let targetTime:CMTime = CMTimeMake(Int64(duration), 1)
//                avPlayer!.seek(to: targetTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
//                avPlayer?.seek(to: targetTime)
//            }
            //
        }
        playPauseImage = playPauseImage?.withRenderingMode(.alwaysTemplate)
        self.playPauseButton.tintColor = UIColor(rgb: 0xFF8D00)
        self.playPauseButton .setImage(playPauseImage, for: .normal)
    }
    
     func getLiveDuration() -> Float {
        var result : Float = 0.0;
        
        if let items = avPlayer?.currentItem?.seekableTimeRanges {
            
            if(!items.isEmpty) {
                let range = items[items.count - 1]
                let timeRange = range.timeRangeValue
                let startSeconds = CMTimeGetSeconds(timeRange.start)
                let durationSeconds = CMTimeGetSeconds(timeRange.duration)
                
                result = Float(startSeconds + durationSeconds)
            }
            
        }
        return result;
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isPlayingNow{
            self.playPauseRadioClicked(nil)
            self.isPlayDisAppear = true;
        }
        self.removeInterruptionNotification()
        //self.unloadLiveAudio()
      
    }
    func createAndLoadHomePageMenu(){
       // arrayHomePageMenuItems
        var vkSideMenuItem = VKSideMenuItem()
        vkSideMenuItem.title = "Radio Bhakti"
        vkSideMenuItem.icon = UIImage(named:"radio_Icon")
        self.arrayHomePageMenuItems.append(vkSideMenuItem)
        
        vkSideMenuItem = VKSideMenuItem()
        vkSideMenuItem.title = "Stories"
        vkSideMenuItem.icon = UIImage(named:"heart_Icon")
        self.arrayHomePageMenuItems.append(vkSideMenuItem)
        
        vkSideMenuItem = VKSideMenuItem()
        vkSideMenuItem.title = "Rangamancha"
        vkSideMenuItem.icon = UIImage(named:"radio_Icon")
        self.arrayHomePageMenuItems.append(vkSideMenuItem)
        
        self.menuTableView.isHidden = false
        self.menuTableView.reloadData();
        
    }
    

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &playbackLikelyToKeepUpContext {
            if (avPlayer?.currentItem!.isPlaybackLikelyToKeepUp)! {
                // loadingIndicatorView.stopAnimating() or something else
                self.playPauseButton.isUserInteractionEnabled = true;
                self.playActivityIndicator.isHidden = true;
                self.playActivityIndicator.stopAnimating();
               self.playPauseRadioClicked(nil)
            } else {
                // loadingIndicatorView.startAnimating() or something else
                self.playPauseButton.isUserInteractionEnabled = false;
              
            }
        }
    }

    public func resumeLive() -> Int {
        guard let livePosition = avPlayer?.currentItem?.seekableTimeRanges.last as? CMTimeRange else {
            return 0
        }
        avPlayer?.seek(to:CMTimeRangeGetEnd(livePosition))
        return 1
    }
    
    func showAudioLoadingView(){
        self.playEquilizer.image = UIImage(named: "pauseEquilizer")
        self.playEquilizer.image = self.playEquilizer.image?.withRenderingMode(.alwaysTemplate)
        playEquilizer.tintColor = UIColor(rgb: 0xFF8D00)
        
        self.playEquilizer.stopSoundPlayImage()
        
        var playPauseImage = UIImage(named: "round")
        playPauseImage = playPauseImage?.withRenderingMode(.alwaysTemplate)
        self.playPauseButton.tintColor = UIColor(rgb: 0xFF8D00)
        self.playPauseButton .setImage(playPauseImage, for: .normal)
        self.playPauseButton.isUserInteractionEnabled = false;
        
        self.playActivityIndicator.color = UIColor.white
        self.playActivityIndicator.isHidden = false;
        self.playActivityIndicator.startAnimating();
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setSlideMenu()
        self.volumeSlider.tintColor = UIColor(rgb: 0xFF8D00)

        self.volumeSlider.thumbTintColor  = UIColor(rgb: 0xFF8D00)
        self.volumeSlider.setThumbImage(UIImage(named: "sliderRadius"), for: .normal)
        self.volumeSlider.setThumbImage(UIImage(named: "sliderRadius"), for: .highlighted)

        super.setLeftNavigationBarItem(imageName: "sliderMenu", titleName: "Radio Choklate", buttonAction: #selector(super.menuTapped))
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: .mixWithOthers)
            print("Playback OK")
            try AVAudioSession.sharedInstance().setActive(true)
            print("Session is Active")
        } catch {
            print(error)
        }
        
     
        self.menuTableView.isHidden = true
        self.radioChkOrignalsLabel.isHidden = true

        self.videoContentTableView.isHidden = true
      
        DispatchQueue.global(qos: .default).async{
                //self.getLandingPageInfo();
        }
          self.addScrollViewContentImages()
        
        
        DispatchQueue.global(qos: .background).async{
            self.fetchVideos(playListItemId: "PL-R2PMSjtm6rJ6qVC9XK9bp9wLdvxliZU")
        }
        
        DispatchQueue.main.async(execute: { () -> Void in
            Timer.scheduledTimer(timeInterval: 3.0,
                                 target: self,
                                 selector: #selector(self.updateBannerImage(_:)),
                                 userInfo: nil,
                                 repeats: true)
        })
        self.loadLiveAudio()
        
    }
    
   override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    if isPlayDisAppear{
        self.playPauseRadioClicked(nil)
        self.isPlayDisAppear = false
    }
    self.addInterruptionNotification()
    }
    
    func unloadLiveAudio(){
        if avPlayer?.rate != 0 && avPlayer?.error == nil{
            avPlayer?.pause()
        }
        self.avPlayer?.removeObserver(self, forKeyPath: "currentItem.playbackLikelyToKeepUp", context: &self.playbackLikelyToKeepUpContext)
        
        if self.avPlayerItem != nil{
            self.avPlayerItem = nil
        }
        if self.avPlayer != nil{
            self.avPlayer = nil
        }
    }
    
    func loadLiveAudio(){
        
        self.showAudioLoadingView();
        
        let urlstring = "http://192.64.80.222:5555/"
        let url = NSURL(string: urlstring)
        //print("playing \(String(describing: url))")
        avPlayerItem = AVPlayerItem.init(url: url! as URL)
        avPlayer = AVPlayer.init(playerItem: avPlayerItem)
        avPlayer?.volume = 1.0
        
        avPlayer?.addObserver(self, forKeyPath: "currentItem.playbackLikelyToKeepUp",
                              options: .new, context: &playbackLikelyToKeepUpContext)
        
        self.volumeImage.image = UIImage(named: "volume")
        self.volumeImage.image = self.volumeImage.image?.withRenderingMode(.alwaysTemplate)
        self.volumeImage.tintColor = UIColor(rgb: 0xFF8D00)
    }
    
    func removeInterruptionNotification(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVAudioSessionInterruption, object: nil)
    }
    func addInterruptionNotification() {
        // add audio session interruption notification
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.handleInterruption(_:)),
                                               name: NSNotification.Name.AVAudioSessionInterruption,
                                               object: nil)
    }
    
    @objc func handleInterruption(_ notification: NSNotification){
        if notification.name != NSNotification.Name.AVAudioSessionInterruption
            || notification.userInfo == nil{
            return
        }
        var info = notification.userInfo!
        var intValue: UInt = 0
        (info[AVAudioSessionInterruptionTypeKey] as! NSValue).getValue(&intValue)
        if let type = AVAudioSessionInterruptionType(rawValue: intValue) {
            switch type {
            case .began:
                // interruption began
                print("began")
            case .ended:
                if isPlayingNow {
                    self.playPauseRadioClicked(nil)
                }
                // interruption ended
                print("ended")
          
            }
        }
    }
    func getLandingPageInfo(){
        let networkManager = NetworkingManager.sharedManager
        let strHomePageImagesUrl =  URLConstants.baseUrl+URLConstants.homePageImages
        
        networkManager.performGetRequest(strHomePageImagesUrl) { (data, HTTPStatusCode, error) -> Void in
            if HTTPStatusCode == 200 && error == nil {
                do{
                    let resultsDict = try JSONSerialization.jsonObject(with: data!, options: []) as! [[String:AnyObject]]
                    print("Result Dict \(resultsDict.debugDescription)")
                    self .parseImageUrls(response: resultsDict)
                } catch {
                    print(error)
                }
            } else{
                 super.displayConnectionErrorAlertView();
                 self.playActivityIndicator.isHidden = true;
            }
        }
    }
    
    func parseImageUrls(response:[[String:AnyObject]]) {
        //self.scrollView.contentSize = CGSize(width: response.count*375, height: self.view.frame.size.height)
        for dictItem in response{
            guard let guid = dictItem["guid"]else {return}
            guard let renderedImageUrl = guid["rendered"] else {return}
            self.arraySliderImageUrls.append(renderedImageUrl as! String)
        }
        
        self.addScrollViewContentImages()
        
    }
    
    @objc func updateBannerImage(_ timer: Timer?) {
        let numberOfPages = (Int)(self.scrollView.contentSize.width/scrollView.frame.size.width)
        if currentPage < numberOfPages {
            currentPage = currentPage + 1
            if currentPage == numberOfPages{
                currentPage = -1
                return
            }
            let newX = CGFloat(currentPage) * scrollView.frame.size.width
            scrollView.setContentOffset(CGPoint(x: newX, y: 0), animated: true)
        } else{
            currentPage = 0
        }
    }
    func addScrollViewContentImages() {
        var lastView:UIView?
        
        let width = self.view.frame.size.width
        let position = 0
         let numberOfImages = self.arraySliderImageUrls.count
         let height = 160
        
            for index in 0..<numberOfImages {
            
            
            
            let view = UIImageView()
            let url = URL(string: self.arraySliderImageUrls[index])
              view.kf.setImage(with: url, placeholder: UIImage(named: "placeHolderLandscape"), options:nil, progressBlock: nil, completionHandler: nil)

            
            scrollView.addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
            scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(position)-[view(\(height))]-\(position)-|", options: [], metrics: nil, views: ["view": view]))
            
            //If view is first then pin the leading edge to main ScrollView otherwise to the last View.
            if lastView == nil && index == 0 {
                scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view(\(width))]", options: [], metrics: nil, views: ["view": view]))
            } else {
                scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[lastView]-0-[view(\(width))]", options: [], metrics: nil, views: ["lastView": lastView!, "view": view]))
            }
            
            //If View is last then pin the trailing edge to mainScrollView trailing edge.
            if index == numberOfImages-1 {
                scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[view]-0-|", options: [], metrics: nil, views: ["view": view]))
            }
            
            //Assign the current View as last view to keep the reference for next View.
            lastView = view
            
        }
        self.updateBannerImage(nil)
        self.addGestureRecognizersToScrollview()
        self.isScrollviewLoaded = true

    }
    
    func fetchVideos(playListItemId:String){
        DispatchQueue.main.async {
            self.activityIndicator.isHidden = false;
            self.view.bringSubview(toFront: self.activityIndicator)
        }
        let videosList = "https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails&playlistId=\(playListItemId)&key=\(URLConstants.apiKey)&maxResults=50"
        
        NetworkingManager.sharedManager.performAsyncGetRequest(videosList) { (data, HTTPStatusCode, error) -> Void in
            if HTTPStatusCode == 200 && error == nil {
                do{
               
                let dictResponse = try JSONSerialization.jsonObject(with: data!, options: []) as! [String:AnyObject]
               // print("Result Dict \(dictResponse.debugDescription)")
                
            //guard let dictResponse = resultsDict else{return}
            let aVideoItemsArray = dictResponse["items"] as! [[String:AnyObject]]
            var arrayVideoIds  = [String]()
            
                    var itemsCount = 0
            for videoItem in aVideoItemsArray{
                let contentDetails = videoItem["contentDetails"] as! [String:AnyObject]
                let videoId = contentDetails["videoId"] as! String
                arrayVideoIds.append(videoId)
            }
            let queue = DispatchQueue(label: "com.fmjockey.queue")
            queue.sync {
                for videoId in arrayVideoIds{
                    let videoInfo =  "https://www.googleapis.com/youtube/v3/videos?part=snippet&id=\(videoId)&key=\(URLConstants.apiKey)"
                    
                    
                    NetworkingManager.sharedManager.performAsyncGetRequest(videoInfo) { (data, HTTPStatusCode, error) -> Void in
                       // DispatchQueue.main.async {
                        itemsCount += 1;
                       
                        if HTTPStatusCode == 200 && error == nil {
                            do{
                                let dictResponse = try JSONSerialization.jsonObject(with: data!, options: []) as! [String:AnyObject]
                               // print("Result Dict \(dictResponse.debugDescription)")

                                let arrayItems = dictResponse["items"] as! [[String:AnyObject]]
                                if arrayItems.count == 0 {return}
                                let snippet = arrayItems[0]["snippet"] as! [String:AnyObject]
                                let localizedContent = snippet["localized"] as! [String:String]
                                let thumbNials = snippet["thumbnails"] as! [String:AnyObject]
                                let vidoItem = VideoItem(videoId: videoId, thumbnailUrl: (thumbNials["medium"] as! [String:AnyObject])["url"] as? String, title: localizedContent["title"] , description:localizedContent["description"])
                                self.arrayVideoItems.append(vidoItem)
                                if itemsCount == aVideoItemsArray.count{
                                    self.videoContentTableView.isHidden = false
                                     self.activityIndicator.isHidden = true;
                                self.videoContentTableView .reloadData()
                                }

                            }    catch {
                                print(error)
                            }
                        } else{
                            super.displayConnectionErrorAlertView();
                            self.playActivityIndicator.isHidden = true;
                        }
                    //}
//                        if(videoId == arrayVideoIds.last){
//                            print(self.arrayVideoItems.debugDescription)
//                            self.videoContentTableView .reloadData()
//                        }

                    }
                    
                    
//                    self.performGetRequestFor(urlString: videoInfo, completionHandler: { dictResponse in
                }
            }
            
        }
             catch {
                print(error)
            }
        }
    }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func addGestureRecognizersToScrollview(){
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        swipeRight.delegate = self
        self.scrollView.addGestureRecognizer(swipeRight)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture(_:)))
        swipeDown.direction = UISwipeGestureRecognizerDirection.left
        swipeDown.delegate = self
        self.scrollView.addGestureRecognizer(swipeDown)
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
                self.updateBannerImagesOnSwipe(isRightSwipe: true)
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
                self.updateBannerImagesOnSwipe(isRightSwipe: false)
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    func updateBannerImagesOnSwipe(isRightSwipe:Bool){
        let numberOfPages = (Int)(self.scrollView.contentSize.width/scrollView.frame.size.width)
        if (!isRightSwipe){
            if currentPage < numberOfPages {
                currentPage = currentPage + 1
                if currentPage == numberOfPages{
                    currentPage = currentPage - 1
                    return
                }
                let newX = CGFloat(currentPage) * scrollView.frame.size.width
                scrollView.setContentOffset(CGPoint(x: newX, y: 0), animated: true)
            }
        } else{
            if(currentPage>0){
                
                if currentPage > 0 {
                    currentPage = currentPage - 1
                    let newX = CGFloat(currentPage) * scrollView.frame.width
                    scrollView.setContentOffset(CGPoint(x: newX, y: 0), animated: true)
                }
            }
        }
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool{
        return true
    }
}

enum VerticalLocation: String {
    case bottom
    case top
}

extension UIView {
    func addShadow(location: VerticalLocation, color: UIColor = .black, opacity: Float = 0.5, radius: CGFloat = 5.0) {
        switch location {
        case .bottom:
            addShadow(offset: CGSize(width: 0, height: 10), color: color, opacity: opacity, radius: radius)
        case .top:
            addShadow(offset: CGSize(width: 0, height: -10), color: color, opacity: opacity, radius: radius)
        }
    }
    
    func addShadow(offset: CGSize, color: UIColor = .black, opacity: Float = 0.5, radius: CGFloat = 5.0) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = radius
    }
}

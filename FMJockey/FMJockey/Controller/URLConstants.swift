//
//  URLConstants.swift
//  FMJockey
//
//  Created by iDev on 29/07/18.
//  Copyright © 2018 Radio Choklate. All rights reserved.
//


extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
class URLConstants {
static let baseUrl = "http://radiochoklateonline.com/wp-json/wp/v2"
    static let homePageImages = "/media?categories=9"
  static  let apiKey = "AIzaSyCfP_nVKf_a11OE841t9OkBbij6_jtV36Q"
   static let loveStoriesSliderImages = "/media?categories=5&mime_type=image/jpeg"
}

class CustomImageView: UIImageView {
    
    // MARK: - Constants
    
    let imageCache = NSCache<NSString, AnyObject>()
    
    // MARK: - Properties
    
    var imageURLString: String?
    
    func downloadImageFrom(urlString: String, imageMode: UIViewContentMode) {
        guard let url = URL(string: urlString) else { return }
        downloadImageFrom(url: url, imageMode: imageMode)
    }
    
    func downloadImageFrom(url: URL, imageMode: UIViewContentMode) {
        //contentMode = imageMode
        if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) as? UIImage {
            self.image = cachedImage
        } else {
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                DispatchQueue.main.async {
                    let imageToCache = UIImage(data: data)
                    self.imageCache.setObject(imageToCache!, forKey: url.absoluteString as NSString)
                    self.image = imageToCache
                }
                }.resume()
        }
    }
}

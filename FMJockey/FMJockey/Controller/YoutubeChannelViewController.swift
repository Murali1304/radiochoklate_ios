//
//  YoutubeChannelViewController.swift
//  FMJockey
//
//  Created by iDev on 29/07/18.
//  Copyright © 2018 Radio Choklate. All rights reserved.
//

import UIKit

enum playState {
    case playing
    case paused
}
extension DispatchQueue {
    static func delay(_ delay: DispatchTimeInterval, closure: @escaping () -> ()) {
        let timeInterval = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: timeInterval, execute: closure)
    }
}

class YoutubeChannelViewController: BaseViewController,UIWebViewDelegate,UIGestureRecognizerDelegate,UITableViewDelegate,UITableViewDataSource  {
     var playPauseTapGesture: UITapGestureRecognizer!
      @IBOutlet weak var videoContentTableView: UITableView!
    @IBOutlet weak var playerView: UIWebView!
    @IBOutlet weak var playPauseButton: UIButton!
    var playerState:playState = .playing
    var currentPlayItemIndex:Int = 0
    var arrayVideoItems:[VideoItem] = [VideoItem]()
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 80

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayVideoItems.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.playVideoFor(videoId: (self.arrayVideoItems[indexPath.row] as VideoItem).videoId!)
        self.playPauseButton.isHidden = true;
    

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        

            let  playListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PlayListTableViewCell", for: indexPath) as! PlayListTableViewCell
         playListTableViewCell.selectionStyle = .none
            let playListItem = self.arrayVideoItems[indexPath.row]
            playListTableViewCell.setVideoCellContent(thumbUrlString: playListItem.thumbnailUrl, videoTitle: playListItem.title!, description: playListItem.description!)
            return playListTableViewCell

        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.playerView.allowsInlineMediaPlayback = true
        self.playerView.mediaPlaybackRequiresUserAction = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
//self.playerView.load(withVideoId: "4WBoGlGYr5w")
        // Do any additional setup after loading the view.
        
        // Set properties
//        self.playerView.allowsInlineMediaPlayback = true
//        self.playerView.mediaPlaybackRequiresUserAction = tr
        // Set properties
        self.playVideoFor(videoId: (self.arrayVideoItems[self.currentPlayItemIndex] as VideoItem).videoId!)
        self.playPauseButton.isHidden = true;
        
        self.playerView.isOpaque = false;
        self.playerView.backgroundColor = UIColor.clear
        
        self.playPauseTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tappedWebView(_:)))
        self.playerView.isUserInteractionEnabled = true
        self.playPauseTapGesture.delegate = self
        self.playerView.addGestureRecognizer(self.playPauseTapGesture)

        super.setLeftNavigationBarItem(imageName: "back", titleName:"Radio Choklate", buttonAction: #selector(super.backTapped))
        self.addGoHomeButtonToNavigationBar()
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool{
        return true
    }
    @objc func tappedWebView(_ gesture: UITapGestureRecognizer?){
        self.playPauseButton.isHidden = false;
        self.view.bringSubview(toFront: self.playPauseButton)
        
        
       let state =  Int(self.stringFromEvaluatingJavaScript(jsToExecute: "ytplayer.getPlayerState()"))
        
        
        if state == 1{
            let state =  self.stringFromEvaluatingJavaScript(jsToExecute: "ytplayer.pauseVideo()")
            self.playerState = .paused
             print("player State \(state)")
            self.playPauseButton.setImage(UIImage(named: "pause"), for: .normal)
        } else{
            let state =  self.stringFromEvaluatingJavaScript(jsToExecute: "ytplayer.playVideo()")
            self.playerState = .playing
             print("player State \(state)")
            self.playPauseButton.setImage(UIImage(named: "play"), for: .normal)
        }
        
        DispatchQueue.delay(.seconds(1)) {
            self.playPauseButton.isHidden = true;
        }
    }
    
    
    @IBAction func PlayerButtonTapped(_ sender: Any) {
 

    }
    func stringFromEvaluatingJavaScript(jsToExecute:String) -> String {
        return self.playerView.stringByEvaluatingJavaScript(from: jsToExecute)!
    }

    func playVideoFor(videoId:String){
        
       let embededHTML = "<html><body style='margin:0px;padding:0px;'><script type='text/javascript' src='http://www.youtube.com/iframe_api'></script><script type='text/javascript'>function onYouTubeIframeAPIReady(){ytplayer=new YT.Player('playerId',{events:{onReady:onPlayerReady}})}function onPlayerReady(a){a.target.playVideo();}</script><iframe id='playerId' type='text/html' width='\(self.view.frame.size.width)' height='\(self.playerView.frame.size.height)' src='http://www.youtube.com/embed/\(videoId)?enablejsapi=1&rel=0&playsinline=1&autoplay=1' frameborder='0'></body></html>"
        
       // let embededHTML = "<html><body style='margin:0px;padding:0px;'><script type='text/javascript' src='http://www.youtube.com/iframe_api'></script><script type='text/javascript'>function onYouTubeIframeAPIReady(){ytplayer=new YT.Player('playerId',{events:{onReady:onPlayerReady}})}function onPlayerReady(a){a.target.playVideo();}</script><iframe id='playerId' type='text/html' width='\(self.view.frame.size.width)' height='\(self.view.frame.size.height)' src='http://www.youtube.com/embed/\(videoId)?enablejsapi=1&rel=0&playsinline=1&autoplay=1' frameborder='0'></body></html>"
        
        // Load your webView with the HTML we just set up
        self.playerView.loadHTMLString(embededHTML, baseURL: Bundle.main.resourceURL)
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("webViewDidFinishLoad")
          self.playerView.mediaPlaybackRequiresUserAction = false
        self.tappedWebView(nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.playerView.delegate = nil
        
    }
    deinit {
     print("de init")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

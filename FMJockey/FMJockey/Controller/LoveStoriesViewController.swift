//
//  LoveStoriesViewController.swift
//  FMJockey
//
//  Created by MuraliKrishna on 14/07/18.
//  Copyright © 2018 Radio Choklate. All rights reserved.
//

import UIKit

class LoveStoriesViewController: BaseViewController,UICollectionViewDataSource,UICollectionViewDelegate ,UIGestureRecognizerDelegate{
let apiKey = "AIzaSyCfP_nVKf_a11OE841t9OkBbij6_jtV36Q"
let channelId = "UCBDppYqUutAQ1B4gaqRhTTg"
    var playListItemsArray = [[String:AnyObject]]()
    var arraySliderImageUrls = [String]()
    var arrayIconImageUrls = [String]()
    var playListInfoArray = [[String:AnyObject]]()
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
     @IBOutlet var collectionView:UICollectionView!
    
    var currentPage:Int = 0

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
       return self.playListItemsArray.count
    }
 

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayListCollectionViewCell", for: indexPath) as! PlayListCollectionViewCell

        let dictPlayListInfo = self.playListItemsArray[indexPath.row]
        cell.setContentFor(imageUrlString:dictPlayListInfo["thumbUrl"] as? String, title:dictPlayListInfo["playlistName"] as? String)
        return cell
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1;
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let playListViewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PlayListViewController") as? PlayListViewController
        playListViewController?.categorieId = self.playListItemsArray[indexPath.row]["catagorieId"] as! String
        playListViewController?.arrayVideoIds = self.playListItemsArray[indexPath.row]["arrayVideoItems"] as! [String]
        playListViewController?.navTitle = self.playListItemsArray[indexPath.row]["playlistName"] as! String
        self.navigationController?.pushViewController(playListViewController!, animated: true)

    }
    func addGestureRecognizersToScrollview(){
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        swipeRight.delegate = self
        self.scrollView.addGestureRecognizer(swipeRight)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture(_:)))
        swipeDown.direction = UISwipeGestureRecognizerDirection.left
        swipeDown.delegate = self
        self.scrollView.addGestureRecognizer(swipeDown)
    }

    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
                self.updateBannerImagesOnSwipe(isRightSwipe: true)
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
                self.updateBannerImagesOnSwipe(isRightSwipe: false)
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    func addScrollViewContentImages() {
        var lastView:UIView?
        
        let width = self.view.frame.size.width
        let position = 0
        let numberOfImages = self.arraySliderImageUrls.count
        let height = 160
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 0)
        for index in 0..<numberOfImages {
            
            let view = UIImageView()
           /// view.contentMode = .scaleAspectFit
            let url = URL(string: self.arraySliderImageUrls[index])
            view.kf.setImage(with: url, placeholder: UIImage(named: "placeHolderLandscape"), options:nil, progressBlock: nil, completionHandler: nil)
            
            scrollView.addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
            scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(position)-[view(\(height))]-\(position)-|", options: [], metrics: nil, views: ["view": view]))
            
            //If view is first then pin the leading edge to main ScrollView otherwise to the last View.
            if lastView == nil && index == 0 {
                scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view(\(width))]", options: [], metrics: nil, views: ["view": view]))
            } else {
                scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[lastView]-0-[view(\(width))]", options: [], metrics: nil, views: ["lastView": lastView!, "view": view]))
            }
            
            //If View is last then pin the trailing edge to mainScrollView trailing edge.
            if index == numberOfImages-1 {
                scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[view]-0-|", options: [], metrics: nil, views: ["view": view]))
            }
            
            //Assign the current View as last view to keep the reference for next View.
            lastView = view
            
        }
        self.updateBannerImage(nil)
         self.addGestureRecognizersToScrollview()
        Timer.scheduledTimer(timeInterval: 3.0,
                             target: self,
                             selector: #selector(self.updateBannerImage(_:)),
                             userInfo: nil,
                             repeats: true)
        //self.isScrollviewLoaded = true
        
        
        
    }
  
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool{
        return true
    }
    
    @objc func updateBannerImage(_ timer: Timer?) {
        let numberOfPages = (Int)(self.scrollView.contentSize.width/scrollView.frame.size.width)
        // let maxX = scrollView.contentSize.width - scrollView.frame.width+100
        // let newX = CGFloat(currentPage) * scrollView.frame.width
        if currentPage < numberOfPages {
            currentPage = currentPage + 1
            if currentPage == numberOfPages{
                currentPage = -1
                return
            }
            let newX = CGFloat(currentPage) * scrollView.frame.size.width
            scrollView.setContentOffset(CGPoint(x: newX, y: 0), animated: true)
        } else{
            currentPage = 0
        }
    }
    
    func updateBannerImagesOnSwipe(isRightSwipe:Bool){
        let numberOfPages = (Int)(self.scrollView.contentSize.width/scrollView.frame.size.width)
        if (!isRightSwipe){
            if currentPage < numberOfPages {
                currentPage = currentPage + 1
                if currentPage == numberOfPages{
                    currentPage = currentPage - 1
                    return
                }
                let newX = CGFloat(currentPage) * scrollView.frame.size.width
                scrollView.setContentOffset(CGPoint(x: newX, y: 0), animated: true)
            }
        } else{
            if(currentPage>0){
                
                if currentPage > 0 {
                    currentPage = currentPage - 1
                    let newX = CGFloat(currentPage) * scrollView.frame.width
                    scrollView.setContentOffset(CGPoint(x: newX, y: 0), animated: true)
                }
            }
        }
    }
    
    func fetchSliderImages(){
        let networkManager = NetworkingManager.sharedManager
        let strHomePageImagesUrl =  URLConstants.baseUrl+URLConstants.loveStoriesSliderImages
        
        networkManager.performGetRequest(strHomePageImagesUrl) { (data, HTTPStatusCode, error) -> Void in
            if HTTPStatusCode == 200 && error == nil {
                do{
                    let resultsDict = try JSONSerialization.jsonObject(with: data!, options: []) as! [[String:AnyObject]]
                    print("Result Dict \(resultsDict.debugDescription)")
                    self .parseImageUrls(response: resultsDict)
                } catch {
                    print(error)
                }
            } else{
                super.displayConnectionErrorAlertView();
                self.activityIndicatorView.isHidden = true
            }
        }
    }
    
    func parseImageUrls(response:[[String:AnyObject]]) {
        //self.scrollView.contentSize = CGSize(width: response.count*375, height: self.view.frame.size.height)
        for dictItem in response{
            guard let guid = dictItem["guid"]else {return}
            guard let renderedImageUrl = guid["rendered"] else {return}
            guard let caption = dictItem["caption"]else {return}
            if caption as! String != "icon"{
               self.arraySliderImageUrls.append(renderedImageUrl as! String)
            } else{
                var dictPlayListItemInfo = [String:AnyObject]()
                guard let slug = dictItem["slug"]else {return}
                if slug as! String == "Love Story"{
                    dictPlayListItemInfo["playlistName"] = slug;
                    dictPlayListItemInfo["playListItemId"] = "PL-R2PMSjtm6o0qWbaYNrGKb0PhFPtQL2k" as AnyObject
                    dictPlayListItemInfo["thumbUrl"] =  renderedImageUrl as AnyObject
                     dictPlayListItemInfo["catagorieId"] =  "6" as AnyObject
            }
               else  if slug as! String == "Kathatia"{
                    dictPlayListItemInfo["playlistName"] = slug;
                    dictPlayListItemInfo["playListItemId"] = "PL-R2PMSjtm6oASNVXxMVh19dBm0I7kJM3" as AnyObject
                    dictPlayListItemInfo["thumbUrl"] =  renderedImageUrl as AnyObject
                    dictPlayListItemInfo["catagorieId"] =  "6" as AnyObject
                }
                else  if slug as! String == "Jibanara Kete"{
                    dictPlayListItemInfo["playlistName"] = "Jibana ra kete ranga" as AnyObject;
                    dictPlayListItemInfo["playListItemId"] = "PL-R2PMSjtm6pn-Gh_rL4o9-C5MSpxUDg5" as AnyObject
                    dictPlayListItemInfo["thumbUrl"] =  renderedImageUrl as AnyObject
                     dictPlayListItemInfo["catagorieId"] =  "12" as AnyObject
                }
                   else  if slug as! String == "Bhaya"{
                dictPlayListItemInfo["playListItemId"] = "PL-R2PMSjtm6qUPAHek_05c0YGbTNh2P6Y" as AnyObject
                dictPlayListItemInfo["thumbUrl"] =  renderedImageUrl as AnyObject
                dictPlayListItemInfo["playlistName"] =  "Bhaya" as AnyObject
                dictPlayListItemInfo["catagorieId"] =  "8" as AnyObject
                }
                else  if slug as! String == "Arjun"{
                    dictPlayListItemInfo["playListItemId"] = "PL-R2PMSjtm6qhEy05B5GVzqx212VS55eg" as AnyObject
                    dictPlayListItemInfo["thumbUrl"] =  renderedImageUrl as AnyObject
                    dictPlayListItemInfo["playlistName"] =  "Ditective Arjun" as AnyObject
                     dictPlayListItemInfo["catagorieId"] =  "14" as AnyObject
                }
                self.playListInfoArray.append(dictPlayListItemInfo)
                if dictPlayListItemInfo.keys.count > 0 {
                    self.fetchVideos(dictPlayListInfo:dictPlayListItemInfo )
                }
            }
            
        }
        
        self.addScrollViewContentImages()
        

        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.activityIndicatorView.isHidden = false;
        DispatchQueue.global(qos: .background).async{
            self.fetchSliderImages()
        }
    
        super.setLeftNavigationBarItem(imageName: "back", titleName: "Stories", buttonAction: #selector(super.backTapped))
         self.addGoHomeButtonToNavigationBar()

    }
    
    func performGetRequestFor(urlString:String, completionHandler:@escaping (_ dictResponse:[String:AnyObject]?) -> Void ){
        let networkManager = NetworkingManager.sharedManager
        
        networkManager.performGetRequest(urlString, completion: { (data, HTTPStatusCode, error) -> Void in
            
            if HTTPStatusCode == 200 && error == nil {
                do{
                    let resultsDict = try JSONSerialization.jsonObject(with: data!, options: []) as! [String:AnyObject]
                    print("Result Dict \(resultsDict.debugDescription)")
                    completionHandler(resultsDict)
                } catch {
                    print(error)
                }
                
            } else{
                super.displayConnectionErrorAlertView();
                self.activityIndicatorView.isHidden = true
            }
        })
    }
    
    func parseChannelInfo(channelInfo:[String:AnyObject]){
        let itemsArray = channelInfo["items"] as! [[String:AnyObject]]
       
        //let myGroup = DispatchGroup()
        
        for item in itemsArray{
            let playListId = item["id"]
            print(playListId as! String)
            let playListSnippet = item["snippet"] as! [String:AnyObject]
            let  playListTitle = (playListSnippet["localized"] as! [String:AnyObject])["title"]
            print(playListTitle as! String)
            let playListThumbUrlString = ((playListSnippet["thumbnails"] as! [String:AnyObject])["default"] as! [String:AnyObject])["url"]
            print(playListThumbUrlString as! String)
            
            
            let playListItem = PlaylistItem(playListId:playListId as? String, playListTitle: playListTitle as? String, playListThumbNailUrl: playListThumbUrlString as? String, arrayVideoItems: nil)
            print("Finished request.")
           // self.playListItemsArray.append(playListItem)
            
          /*  let videosList = "https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails&playlistId=\(playListId as! String)&key=\(apiKey)&maxResults=50"
            
            
            //myGroup.enter()
            var videoItemsArray = [VideoItem]()
            
            //let semaphore = DispatchSemaphore(value: 0)
            
            self.performGetRequestFor(urlString: videosList) { dictResponse in
                guard let dictResponse = dictResponse else{return}
                let aVideoItemsArray = dictResponse["items"] as! [[String:AnyObject]]
                for videoItem in aVideoItemsArray{
                    let contentDetails = videoItem["contentDetails"] as! [String:AnyObject]
                    let videoId = contentDetails["videoId"] as! String
                    
                  let videoModel = VideoItem(videoId: videoId, thumbnailUrl: nil)
                    videoItemsArray.append(videoModel)
                }
                let playListItem = PlaylistItem(playListId:playListId as? String, playListTitle: playListTitle as? String, playListThumbNailUrl: playListThumbUrlString as? String, arrayVideoItems: videoItemsArray)
                print("Finished request.")
                self.playListItemsArray.append(playListItem)
                //semaphore.signal() // 2
               // myGroup.leave()
            }
            
            let timeout = DispatchTime.now() + .seconds(15)
//            if semaphore.wait(timeout: timeout) == .timedOut { // 3
//                //XCTFail("\(urlString) timed out")
//                 print("TimeOut Error")
//            }
    */
        }
      //  myGroup.notify(queue: .main) {
           // print(self.playListItemsArray)

        self.collectionView.reloadData()
      //  }`
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
       // https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails&playlistId=PL-R2PMSjtm6qi5rLLK7LznNbsuQHtZOGz&key=
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */



    func fetchVideos(dictPlayListInfo:[String:AnyObject]){
        var dictPlayListInfo = dictPlayListInfo
        let  playListItemId = dictPlayListInfo["playListItemId"] as! String
       
    let videosList = "https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails&playlistId=\(playListItemId)&key=\(URLConstants.apiKey)&maxResults=50"
    
    NetworkingManager.sharedManager.performAsyncGetRequest(videosList) { (data, HTTPStatusCode, error) -> Void in
        if HTTPStatusCode == 200 && error == nil {
            do{
                
                let dictResponse = try JSONSerialization.jsonObject(with: data!, options: []) as! [String:AnyObject]
                let aVideoItemsArray = dictResponse["items"] as! [[String:AnyObject]]
                var arrayVideoIds  = [String]()
                
               // var itemsCount = 0
                for videoItem in aVideoItemsArray{
                    let contentDetails = videoItem["contentDetails"] as! [String:AnyObject]
                    let videoId = contentDetails["videoId"] as! String
                    arrayVideoIds.append(videoId)
                }
                 dictPlayListInfo["arrayVideoItems"] = arrayVideoIds as AnyObject
                self.playListItemsArray.append(dictPlayListInfo)
                
                self.collectionView.reloadData()
                 self.activityIndicatorView.isHidden = true;
                
               // print(arrayVideoIds)
               // var  arrayPlayListVideoInfo = [VideoItem]()
                
                 //   for videoId in arrayVideoIds{
                        
                        
                     //   let videoInfo =  "https://www.googleapis.com/youtube/v3/videos?part=snippet&id=\(videoId)&key=\(URLConstants.apiKey)"
                        
//                        NetworkingManager.sharedManager.performGetRequest(videoInfo) { (data, HTTPStatusCode, error) -> Void in
//                            // DispatchQueue.main.async {
//                            itemsCount += 1;
//
//                            if HTTPStatusCode == 200 && error == nil {
//                                do{
//                                    var dictResponse = try JSONSerialization.jsonObject(with: data!, options: []) as! [String:AnyObject]
//                                    // print("Result Dict \(dictResponse.debugDescription)")
//
//                                    let arrayItems = dictResponse["items"] as! [[String:AnyObject]]
//                                    if arrayItems.count == 0 {return}
//                                    let snippet = arrayItems[0]["snippet"] as! [String:AnyObject]
//                                    let localizedContent = snippet["localized"] as! [String:String]
//                                    let thumbNials = snippet["thumbnails"] as! [String:AnyObject]
//                                    let vidoItem = VideoItem(videoId: videoId, thumbnailUrl: (thumbNials["medium"] as! [String:AnyObject])["url"] as? String, title: localizedContent["title"] , description:localizedContent["description"])
//                                    arrayPlayListVideoInfo.append(vidoItem)
//
//                                    if itemsCount == aVideoItemsArray.count{
//                                        dictResponse["arrayVideoItems"] = arrayPlayListVideoInfo as AnyObject
//                                        self.playListItemsArray.append(dictResponse)
//                                    }
//
//                                }    catch {
//                                    print(error)
//                                }
//                            }
//
//                        }
                        
                   // }
                }
                
            catch {
                print(error)
            }
        }
}
}
}
